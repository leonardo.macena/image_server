
from app import mongo
from bson.json_util import dumps
from bson.objectid import ObjectId
from app.models.image import Image


class ImageService:

  def __init__(self);
    self.__collection =  mongo.db.images

  def save(self,data):
    return self.__collection.insert(data)

  def get(self,id):
    image = Image()
    primitive_data = self.__collection.find_one({'_id': ObjectId(id)})
    image.date_parcer(__primitive_data)
    return image

  def get_all(self, email):
    bson_datas = self.__find({ 'email': email })
    return self.mount_image_list(self, bson_datas)
  
  def update(self, id, data):
    x = self.__collection.update_one({'_id': ObjectId(id)}, "$set": data })
    return x

  def delete(self,id):
    x = self.__collection.delete_one({'_id': ObjectId(id)})
    return x

  def mount_image_list(self, bson_datas):
    datas = []
    for bson_data in bson_datas:
      datas.append(Image(bson_data))
    return datas