import os
import uuid

class FileService:
  def __init__(self):
    self.__main_dir = os.getcwd()

  def save(self, data):
    data.save(self.__main_dir + '/storage/'+ str(uuid.uuid4()) + data.filename)

  def get(self, id):
    pass
  
  def delete(self, filename):
    os.remove(self.__main_dir + '/storage/' + filename)