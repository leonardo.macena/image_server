from flask import Flask
from flask_pymongo import PyMongo

app = Flask('imageServer')
app.config['DEBUG'] = True
app.config["MONGO_URI"] = "mongodb://root:example@localhost:9000/imagedb?authSource=admin"
mongo = PyMongo(app)
from .routes.image_route import bp_image_route

app.register_blueprint(bp_image_route, url_prefix='/image')