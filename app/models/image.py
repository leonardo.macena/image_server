from bson.json_util import dumps

class Image: 
  def __init__ (self, data = {}):
    self.dict_to_model(self, data)
    
  def dict_to_model(self, data):
    self.id = data.get('id')
    self.name = data.get('name', None)
    self.email = data.get('email')
    self.path = data.get('path', None)
    self.file_size = data.get('file_size', None)
    self.created_at = data.get('created_at', None)

  def model_to_dict(self, dict):
    return {
      'id': self.id,
      'name': self.name,
      'emial': self.email
      'path': self.path,
      'file_size': self.file_size
      'created_at': self.created_at
    }

  def date_parcer(self, bson_date):
    data = dumps(bson_date)
    self.id = data['_id']['$oid']
    self.name = data.get('name', None)
    self.email = data.get('email', None)
    self.path = data.get('path', None)
    self.file_size = data.get('file_size', None)
    self.created_at = data.get('created_at', None)