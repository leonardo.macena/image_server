from flask import Blueprint, jsonify, request, send_file
from app import mongo
from bson.json_util import dumps
import os
from pathlib import Path


bp_image_route = Blueprint('bp_image_route', __name__)

image_collection = mongo.db.images

@bp_image_route.route('test', methods=['POST'])
def create ():
  payload = request.get_json()
  image_collection = mongo.db.images
  x = image_collection.insert(payload)
  print(dumps(x))
  return jsonify({'message': 'Ok'}), 200

@bp_image_route.route('', methods=['POST'])
def save ():
  test = os.getcwd()
  print(test)
  if request.files:
    print(request.files)
    image = request.files['image']
    print(dir(image))
    print('////////////////////')
    print(dir(image.stream))
    image.save(os.path.join(test + '/storage', image.filename))
    print(image.filename)
    return 'test', 200
  else:
    return 'error', 404
  

@bp_image_route.route('', methods=['GET'])
def get_all():
  folder = os.getcwd()
  return send_file(folder + '/storage/' +  'Barnabas-Collins-tim-burtons-dark-shadows-29774056-350-338.jpg')
  